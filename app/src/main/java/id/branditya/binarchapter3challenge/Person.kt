package id.branditya.binarchapter3challenge

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Person(
    val age : Int?,
    val address : String?,
    val job : String?
) : Parcelable
