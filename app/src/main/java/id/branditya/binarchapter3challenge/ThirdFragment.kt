package id.branditya.binarchapter3challenge

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.findNavController
import id.branditya.binarchapter3challenge.databinding.FragmentThirdBinding

class ThirdFragment : Fragment() {
    private var _binding : FragmentThirdBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(REQUEST_KEY) { _, bundle ->
            val person : Person= bundle.getParcelable<Person>(KEY_PARCEL) as Person
            val age = person.age
            val address = person.address
            val job = person.job
            if (age != null) {
                when {
                    age == 0 -> {
                        binding.tvAge.text = age.toString()
                        binding.tvAge.visibility = View.GONE
                    }
                    age % 2 == 0 -> {
                        binding.tvAge.text = "Usia : $age bernilai Genap"
                        binding.tvAge.visibility = View.VISIBLE
                    }
                    else -> {
                        binding.tvAge.text = "Usia : $age bernilai Ganjil"
                        binding.tvAge.visibility = View.VISIBLE
                    }
                }
            }

            if (address != ""){
                binding.tvAddress.text = "Alamat : $address"
                binding.tvAddress.visibility = View.VISIBLE
            } else {
                binding.tvAddress.visibility = View.GONE
            }

            if (job != ""){
                binding.tvJob.text = "Pekerjaan : $job"
                binding.tvJob.visibility = View.VISIBLE
            } else {
                binding.tvJob.visibility = View.GONE
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentThirdBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val name = ThirdFragmentArgs.fromBundle(arguments as Bundle).keyName
        binding.tvName.text = "Nama : $name"
        binding.btn3.setOnClickListener {
            it.findNavController().navigate(R.id.action_thirdFragment_to_fourthFragment)
        }
    }

    companion object{
        const val REQUEST_KEY = "request_key"
        const val KEY_PARCEL = "parcel"
    }
}