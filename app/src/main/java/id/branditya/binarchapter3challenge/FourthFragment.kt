package id.branditya.binarchapter3challenge

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.setFragmentResult
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import id.branditya.binarchapter3challenge.ThirdFragment.Companion.KEY_PARCEL
import id.branditya.binarchapter3challenge.ThirdFragment.Companion.REQUEST_KEY
import id.branditya.binarchapter3challenge.databinding.FragmentFourthBinding

class FourthFragment : Fragment() {
    private var _binding : FragmentFourthBinding? = null
    private val binding get () = _binding!!

    private var age : Int = 0
    private var address : String = ""
    private var job : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this,backPressedCallback)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFourthBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btn4.setOnClickListener {
            sendDataBack(it)
        }
    }

    private val backPressedCallback = object :OnBackPressedCallback(true){
        override fun handleOnBackPressed() {
            setResult()
            requireActivity().findNavController(R.id.fragmentContainerView).popBackStack()
        }
    }

    private fun setResult() {
        age = if (binding.etAge.text.isEmpty()){
            0
        } else{
            binding.etAge.text.toString().toInt()
        }

        address = if (binding.etAddress.text.isEmpty()){
            ""
        } else{
            binding.etAddress.text.toString()
        }

        job = if (binding.etJob.text.isEmpty()){
            ""
        } else{
            binding.etJob.text.toString()
        }
        val bundle = Bundle()
        val person = Person(age , address, job)
        bundle.putParcelable(KEY_PARCEL, person)
        setFragmentResult(REQUEST_KEY, bundle)
    }

    private fun sendDataBack(it:View) {
        setResult()
        it.findNavController().popBackStack()
    }

}